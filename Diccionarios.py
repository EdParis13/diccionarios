# Diccionarios
# Ejemplo, titulos de cada equipo de la lpf
# LOS EQUIPOS SON LLAVES, LOS TITULOS SON VALORES
if __name__ == "__main__":
    equipos_lpf = {
        'Arabe Unido': 15,
        'CAI': 2,
        'Tauro': 13,
        'San Francisco': 10,
        'Plaza Amador': 7,
        'Universitarios': 2,
        'Sporting SM': 1
    }
    print(equipos_lpf)

    print(equipos_lpf['CAI'])

    equipos_lpf['Alianza FC'] = 0  # Se agrega el equipo Alianza al diccionario.
    print(equipos_lpf)
    equipos_lpf['Alianza FC'] = 1  # Al agregar al mismo equipo con otro valor, solo cambia el valor de 0 a 1
    print(equipos_lpf)

    del equipos_lpf['Alianza FC']  # El del elimina un valor, en este caso, elimina el registor de Alianza
    print(equipos_lpf)

    print(
        equipos_lpf.get('Arabe Unido'))  # El get busca la infromacion del valor seleccionado, en este caso Arabe Unido
    print(equipos_lpf.get('Atletico Veraguense'))

    e = equipos_lpf.items()  # El items, crea duplas dentro de una lista.
    print(e)

    e1 = equipos_lpf.keys()  # El keys muestra una lista de los equipos solamente
    print(e1)

    e2 = equipos_lpf.values()  # El values muetra una lista de los titulos de cada equipo
    print(e2)

    equipos_lpf.pop('CAI')
    print(equipos_lpf)

    equipos_lpf.clear()  # El clear, deja un diccionario vacio
    print(equipos_lpf)